'use strict'

angular.module('Kioskapp')


.controller('NoScanProductsController', ['$scope','ProductService',"$state",'LoaderService','$rootScope', function($scope,ProductService,$state,LoaderService, $rootScope){

    
    var product = new ProductService()  
    $scope.api = $rootScope.api  
    LoaderService.show("Loading Products")

    product.productslist()
    .then(function(data){
        LoaderService.hide()
        $scope.products = data
    })  

   $scope.get_product = function (id) {
    LoaderService.show("Adding item")
    

        var promise = product.get_noscanproduct(id)
        promise.then(function(data){
            LoaderService.hide()
            $state.go('tab.cart')

        }, function(data){
            console.log(data)
            if (data == "not a product"){
                LoaderService.hide()
                alert("product not found")
            }
        })
    }


        
}])