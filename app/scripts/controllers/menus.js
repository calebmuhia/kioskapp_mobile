'use strict'

angular.module('Kioskapp')


.controller('ContentController', ['$scope','$ionicSideMenuDelegate', function($scope, $ionicSideMenuDelegate){

    $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
    
}])