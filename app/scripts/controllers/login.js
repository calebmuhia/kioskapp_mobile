'use strict'

angular.module('Kioskapp')

.controller('LoginCtrl', ['$scope','barcodeScanner','localStorageService','LoaderService','$rootScope','$http','authService','$state','$ionicSlideBoxDelegate', function($scope,barcodeScanner,localStorageService,LoaderService,$rootScope,$http, authService,$state,$ionicSlideBoxDelegate){
    $scope.slideHasChanged = function($index) {
        // $ionicSlideBoxDelegate.slide($index, 500);
        // console.log(index)
    }

    $scope.checkingLogin = true

    $rootScope.globals = {};



    
    
    $scope.loadUserData = function(){
        LoaderService.show('Login In User');
        $http.get($rootScope.api+"/api/v3/user/")
        .then(function(data){
          console.log()

          $rootScope.currentUser = data.data.objects[0]
          authService.loginConfirmed()

        }, function(error){
          LoaderService.hide()
          console.log('error', error)
        })

        

    }

    $scope.$on('event:auth-loginRequired', function() {
      LoaderService.hide()

    });

   



    

    

    $scope.$on('event:auth-loginConfirmed', function() {
      console.log('loginConfirmed' )

          LoaderService.hide()
          $state.go('tab.cart')
        });




    $scope.loadUserData()


    // $scope.scan = function(){
    //   var result = '{"user":"test","secret":"d257765751e5e02207f2dbfd0cbee6e4e0fe1d0c","api":"http://li701-9.members.linode.com:9009","id":"deae44e55dee44f022de","location":"grenville"}';
         
    //     var text = angular.fromJson(result)

    //     console.log(typeof(text))
    //     console.log(text)

        
    //     var data = "client_id="+text.id+"&client_secret="+text.secret+"&grant_type=password&username="+text.user+"&password="+text.user+"&scope=write" 
    //     console.log(data)
    //       $http({
    //         url:text.api+'/oauth2/access_token',
    //         method:"POST",
    //         headers:{
    //           "Content-Type":"application/x-www-form-urlencoded"
    //         },
    //         data:data
    //       })
    //       .success(function(data){
    //         console.log(data)
    //         $rootScope.globals.access_token = data.access_token
    //         $rootScope.api = text.api
    //         localStorageService.set('access_token', data.access_token)
    //         localStorageService.set('api', text.api)
            
    //         $http.defaults.headers.common['Authorization'] = 'OAuth '+data.access_token;
    //         $scope.loadUserData()
    //       }) 

    // }

    $scope.scan = function () {
      barcodeScanner.scan().then( function (result) {
        
        if (result.canceled) {
          return;
        }

        var text = angular.fromJson(result.text)

        console.log(typeof(text))
        console.log(text)

        
        var data = "client_id="+text.id+"&client_secret="+text.secret+"&grant_type=password&username="+text.user+"&password="+text.user+"&scope=write" 
        console.log(data)
          $http({
            url:text.api+'/oauth2/access_token',
            method:"POST",
            headers:{
              "Content-Type":"application/x-www-form-urlencoded"
            },
            data:data
          })
          .success(function(data){
            console.log(data)
            $rootScope.globals.access_token = data.access_token
            $rootScope.api = text.api
            localStorageService.set('access_token', data.access_token)
            localStorageService.set('api', text.api)
            
            $http.defaults.headers.common['Authorization'] = 'OAuth '+data.access_token;
            $scope.loadUserData()
          }) 



        // text from qr code or barcode is contained in result.text
      }, function (err) {
        alert(err);
      });
    };
    
}])