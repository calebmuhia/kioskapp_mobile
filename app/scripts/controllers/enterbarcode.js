'use strict'

angular.module('Kioskapp')


.controller('EnterBarcodeController', ['$scope','barcodeScanner','ProductService',"$state",'LoaderService', function($scope,barcodeScanner,ProductService,$state,LoaderService){

    var product = new ProductService();

    $scope.barcode = {num:''};




   $scope.enterBarcode = function () {
    console.log($scope.barcode)
    
      
        LoaderService.show("adding Item")
      
        var promise = product.get_product($scope.barcode.num)
        promise.then(function(data){
            LoaderService.hide()
            $state.go('tab.cart')

        }, function(data){
            console.log(data)
            if (data == "not a product"){
                LoaderService.hide()
                alert("product not found")
            }
        })
      }


    
}])