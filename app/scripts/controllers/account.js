'use strict'

angular.module('Kioskapp')

.controller('AccountCtrl', ['$scope','$rootScope','User','LoaderService','$ionicModal','Order','localStorageService','$location','$http', function($scope,$rootScope,User,LoaderService,$ionicModal,Order,localStorageService,$location,$http){
     var user = new User()
    $scope.saveUser = function(){
        LoaderService.show('Saving User Data')

        user.save_user($rootScope.currentUser)
        .then(function(data){
            LoaderService.hide()
            $rootScope.currentUser = data
        },function(error){
            LoaderService.hide()
            alert("error saving, Try Again")
            console.log(error)
        })
    };

    $scope.logout = function(){
        localStorageService.clearAll()
        
        $http.defaults.headers.common['Authorization'] = '';
 

        $location.path('/login');

    };
    
    var order = new Order()

    $scope.get_orders = function(){
        order.get_orders()
        .then(function(response){
            $scope.orders = response.objects
            $scope.meta = response.meta
        })

    }
    $scope.get_orders()
    $scope.getting_orders = 'disable';

    $scope.get_next = function(){
        $scope.getting_orders = 'disable';
        order.next_orders($scope.meta.next)
        .then(function(response){
            $scope.orders.push.apply($scope.orders, response.objects)
            $scope.getting_orders = '';

            $scope.meta = response.meta
        })

    }



    // Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('templates/order_modal.html', function($ionicModal) {
        $scope.modal = $ionicModal;
    }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    }); 


  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };


    
}])