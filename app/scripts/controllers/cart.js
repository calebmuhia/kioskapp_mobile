'use strict';
angular.module('Kioskapp')

.controller('CartCtrl', ['$scope','$rootScope','ngCart','LoaderService','Cart','$state', function($scope,$rootScope,ngCart,LoaderService,Cart,$state) {

    var cart = new Cart()
    var server_cart = {};

    $scope.initCart = function(){
        LoaderService.show('Loading Cart');
        cart.get_items()
        .then(function(fetched_cart){
            $scope.server_cart = fetched_cart
            console.log(fetched_cart)
            LoaderService.hide()
        })
    }

    $scope.checkout_account = function(){
        LoaderService.show('Checking Out Please wait ...');
        
        var promise = cart.checkout_account()
        promise.then(function(response){
            LoaderService.hide();
            
            if (response.status == 'success'){
                
                ngCart.empty();

                $state.go('thankyou')


            }
            else{
                $state.go('insufficient_funds')
            }
            

        }, function(error){
            
        })
    }
    $rootScope.$on('ngCart:itemRemoved', function(event, item){

        LoaderService.show("Removing Item");
        console.log(item)
        var promise = cart.delete_item(item._id)
        promise.then(function(fetched_cart){
            $scope.server_cart = fetched_cart
            console.log(fetched_cart)
            LoaderService.hide()

        }, function(error){
            console.log(error)
            ngCart.addItem(item._id, item._name,parseFloat(item._price),parseInt(item._quantity),item._data) 
            LoaderService.hide()
        })
    })



    
    $scope.$watch('server_cart', function(server_cart){

        try{
            
            ngCart.empty();

            angular.forEach(server_cart.taxes, function(val, ind){
                if (val[0] == 'Gst'){
                    ngCart.setHst(parseFloat(val[1]))
                }
                else if(val[0] == 'Pst'){
                    ngCart.setPst(parseFloat(val[1]))

                }
            })
           
            ngCart.setCartTotal(parseFloat(server_cart.cart_totals))

            angular.forEach(server_cart.items, function(val, ind){   
                ngCart.addItem(val.cart_item, val.name,parseFloat(val.price),parseInt(val.quantity),val) 
        })

        }

        catch(e){
            console.log(e)
        }

        
    })
}]);
