'use strict'

angular.module('Kioskapp')

.controller('InsufficientFundsCtrl', ['$scope','$rootScope','User', function($scope, $rootScope, User){

    var user = new User()

    var promise = user.getAccount()
    promise.then(function(response){
        $rootScope.currentUser = response
    })
}])