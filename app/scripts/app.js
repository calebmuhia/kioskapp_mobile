'use strict';
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('Kioskapp', ['ionic', 'LocalStorageModule','http-auth-interceptor', 'ngCart'])

.run(['$ionicPlatform','$rootScope', '$location', 'localStorageService', '$http',function($ionicPlatform,$rootScope, $location, localStorageService, $http) {
    var access_token= localStorageService.get('access_token') || undefined;
    console.log(access_token)
    if (access_token) {
        $http.defaults.headers.common['Authorization'] = 'OAuth '+access_token; // jshint ignore:line
    }

    var api = localStorageService.get('api') || undefined;
    console.log(api)

    if (api) {
      $rootScope.api =  api
    }
    



    $rootScope.$on('$locationChangeStart', function (newRoute, oldRoute) {
        //redirect to login page if not logged in
        if ($location.path() !== '/login' &&  !$rootScope.currentUser ) {
            $location.path('/login');
        }
        
    });

    $ionicPlatform.registerBackButtonAction(function () {
  
    navigator.app.exitApp();
}, 100);

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
}])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // setup an abstract state for the tabs directive

    .state('login',{
      url:'/login',
      templateUrl:'templates/login.html',
      controller:'LoginCtrl'

    })

    

    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    .state('thankyou',{
      url:'/thankyou',
      templateUrl:'templates/thankyou.html',
      controller:'ThankYouCtrl'
    })
    .state('insufficient_funds',{
      url:'/insufficient_funds',
      templateUrl:'templates/insufficient_funds.html',
      controller:'InsufficientFundsCtrl'
    })


    .state('tab.cart', {
      url: '/cart',
      views: {
        'tab-cart': {
          templateUrl: 'templates/tab-cart.html',
          controller: 'CartCtrl'
        }
      }
    })

    .state('tab.product', {
      url: '/product',
      views: {
        'tab-cart': {
          templateUrl: 'templates/tab-products.html',
          controller: 'ProductsController'
        }
      }
    })
    .state('tab.enterbarcode', {
      url: '/enterbarcode',
      views: {
        'tab-cart': {
          templateUrl: 'templates/enterbarcode.html',
          controller: 'EnterBarcodeController'
        }
      }
    })
    .state('tab.noscanproducts', {
      url: '/noscanproducts',
      views: {
        'tab-cart': {
          templateUrl: 'templates/tab-noscanproducts.html',
          controller: 'NoScanProductsController'
        }
      }
    })

    .state('tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl'
        }
      }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

})

.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('Kioskapp')
    .setStorageType('localStorage')
    .setNotify(true, true)
});

