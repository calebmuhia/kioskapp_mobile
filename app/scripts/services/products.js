'use strict'

angular.module('Kioskapp')


.factory('ProductService', ['$http','$q','$rootScope','Cart', function($http, $q, $rootScope,Cart){

   var ProductService = function(){

   }

   var cart = new Cart()

   ProductService.prototype.get_product = function(barcode){

    var deffered = $q.defer()

    $http.get($rootScope.api+'/api/v3/product/scan_barcode/'+barcode+'/')
    .then(function(response){
      console.log(response)
      if(response.data.type == 'not a product'){

        deffered.reject("not a product")
      }
      else if(response.data.type == 'PRODUCT'){
        cart.add_item(response.data.item_id)
        .then(function(data){

          deffered.resolve(data.data)

        },
        function(error){
          deffered.reject(error)
        });



      }
    });


return deffered.promise

   }

   ProductService.prototype.productslist = function(){
    var deffered = $q.defer()
      $http.get($rootScope.api+'/api/v3/noscanproduct/')
      .then(function(response){
        deffered.resolve(response.data.objects)

      })
      return deffered.promise
   }

    ProductService.prototype.get_noscanproduct = function(id){

    var deffered = $q.defer()

    
        cart.add_item(id)
        .then(function(data){

          deffered.resolve(data.data)

        },
        function(error){
          deffered.reject(error)
        });




      return deffered.promise

   }

   return ProductService

}])