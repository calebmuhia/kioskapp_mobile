angular.module('Kioskapp')
.factory('LoaderService', function($rootScope, $ionicLoading) {
  return {
        show : function(msg) {

            $rootScope.loading = $ionicLoading.show({

              // The text to display in the loading indicator
              content: '<i class="icon ion-looping"></i> '+msg,

              // The animation to use
              animation: 'fade-in',

              // Will a dark overlay or backdrop cover the entire view
              showBackdrop: true,

              // The maximum width of the loading indicator
              // Text will be wrapped if longer than maxWidth
              maxWidth: 200,

              // The delay in showing the indicator
              showDelay: 10
            });
        },

        hide : function(){
            $ionicLoading.hide();
        }
    }
});