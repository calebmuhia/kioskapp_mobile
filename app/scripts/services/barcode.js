'use strict';

angular.module('Kioskapp')


.factory('barcodeScanner',[
    '$q',
    '$rootScope',
    '$window',
    
    function ($q, $rootScope, $window) {
        //
        var scanner = {};

        /**
         * [scan description]
         * @return {[type]} [description]
         */
        scanner.scan = function () {
            var deferred = $q.defer();

            if (cordova.plugins && cordova.plugins.barcodeScanner) {
                cordova.plugins.barcodeScanner.scan( function (result) {
                    // success callback
                    
                        deferred.resolve(result);
                    
                }, function (error) {
                    console.log(error)
                    // error callback
                    $rootScope.$broadcast('error', 'scanFailed');
                    
                    deferred.reject('scanFailed');
                    
                });
            } else {
                // scanner functions are not defined
                $rootScope.$broadcast('error', 'undefined');
                
                    deferred.reject('undefined');
                
            }

            return deferred.promise;
        };

        return scanner;
    }]);