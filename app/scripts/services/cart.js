'use strict'

angular.module('Kioskapp')


.factory('Cart', ['$http','$q','$rootScope', function($http, $q, $rootScope){

    console.log('rootScope api', $rootScope.api)


    

    var Cart  = function(){
        this.items = [];
        this.taxes = [];
        this.sub_total = '';
        this.total = '';
        this.is_loaded = false

    }

    Cart.prototype.setItems = function(items){
        this.items = items

    }

    Cart.prototype.get_items = function(){

        var self = this
        

        return $http.get($rootScope.api+'/api/v3/cart/get_cartitems/')
        .then(function(response){
            return response.data
            
            
        }, function(error){
            return error
        })

    } 

    Cart.prototype.add_item = function(product_id){
        var self = this
        return $http.get($rootScope.api+'/api/v3/cart/add_item/'+product_id+'/')
        .then(function(response){

            return response.data
        }, function(error){
            return error
        })

    } 
    Cart.prototype.delete_item = function(cartitem){
        var self = this
        var defered = $q.defer()
        $http.get($rootScope.api+'/api/v3/cart/delete_item/'+cartitem+'/')
        .then(function(response){
            if (response.data !=''){
                console.log('resolved', response)
                defered.resolve(response.data)
            }
            else{
                console.log('not resolved', response)
                defered.reject(response)
            }

            
           
        }, function(error){
            console.log('not resolved', error)
            defered.reject(error)
        })

        return defered.promise

    }

    Cart.prototype.checkout_account = function(){

        var self = this
        var defered = $q.defer()
        $http.get($rootScope.api+'/api/v3/order/checkout_account/')
        .then(function(response){
            if (response.data !=''){
                defered.resolve(response.data)
            }
            else{
                
                defered.reject(response)
            }

            
           
        }, function(error){
            
            defered.reject(error)
        })

        return defered.promise

    }  

    return Cart
}])
