'use strict'

angular.module('Kioskapp')


.factory('User', ['$http','$q','$rootScope', function($http, $q, $rootScope){
    

    var User  = function(){
        

    }

    User.prototype.setAccount = function(items){
        this.items = items

    }

    User.prototype.save_user = function(user){
        var self = this
        var deffered = $q.defer()
        

        $http.put($rootScope.api+user.resource_uri,user)
        .then(function(data){
            console.log(data)
          
          deffered.resolve(data.data)

          
        }, function(error){
          deffered.reject(error)
        })

        return deffered.promise
    }

    User.prototype.getAccount = function(){

        var self = this
        var deffered = $q.defer()
        

        $http.get($rootScope.api+"/api/v3/user/")
        .then(function(data){
            console.log(data)
          
          deffered.resolve(data.data.objects[0])

          
        }, function(error){
          deffered.reject(error)
        })

        return deffered.promise

    } 

    

    
    return User
}])
