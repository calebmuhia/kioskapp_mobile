'use strict'

angular.module('Kioskapp')


.factory('Order', ['$http','$q','$rootScope', function($http, $q, $rootScope){
    

    var Order  = function(){
        

    }

    Order.prototype.get_orders = function(){

        var deffered = $q.defer()
        

        $http.get($rootScope.api+"/api/v3/order/?limit=15")
        .then(function(data){
            console.log(data)
          
          deffered.resolve(data.data)

          
        }, function(error){
          deffered.reject(error)
        })

        return deffered.promise

    }
    Order.prototype.next_orders = function(url){
        var deffered = $q.defer()
        $http.get($rootScope.api+url)
        .then(function(data){
            console.log(data)

          
          deffered.resolve(data.data)

          
        }, function(error){
          deffered.reject(error)
        })

        return deffered.promise

    }

    
    

    
    return Order
}])
